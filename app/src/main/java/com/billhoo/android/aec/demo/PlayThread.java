package com.billhoo.android.aec.demo;

import java.util.ArrayList;

import android.media.AudioFormat;
import android.media.AudioManager;
import android.media.AudioRecord;
import android.media.AudioTrack;
import android.media.MediaRecorder;

import com.android.webrtc.audio.MobileAEC;

public class PlayThread extends Thread{

	MobileAEC aecm;

	private static final int SAMPLE_RATE = 8000;
	private ArrayList<short[]> mBufferList;
	
	private AudioTrack mAudioTrack;
	public void StartPlay(ArrayList<short[]> buffers)
	{
		mBufferList = buffers;
		
		int minBufSize = AudioRecord.getMinBufferSize(SAMPLE_RATE, AudioFormat.CHANNEL_IN_MONO, AudioFormat.ENCODING_PCM_16BIT);

		mAudioTrack = new AudioTrack(
				AudioManager.STREAM_MUSIC,
				SAMPLE_RATE,
				AudioFormat.CHANNEL_CONFIGURATION_MONO,
				AudioFormat.ENCODING_PCM_16BIT,
				minBufSize,
				AudioTrack.MODE_STREAM);

		start();
	}
	
	public void run()
	{
		int type = mAudioTrack.getStreamType();

		mAudioTrack.play();

		for(short[] buf : mBufferList)
		{
//			short[] aecTmpOut = new short[80];

			if (this.aecm != null)
			{
				try {
					aecm.farendBuffer(buf, 80);
				}
				catch (Exception e)
				{
					int k = 0;
				}
			}

			mAudioTrack.write(buf, 0, buf.length);


		}
		mAudioTrack.stop();
	}

}
