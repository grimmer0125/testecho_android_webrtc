package com.billhoo.android.aec.demo;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;

import android.app.Activity;
import android.os.Bundle;
import android.os.Environment;

import com.android.webrtc.audio.MobileAEC;
import com.android.webrtc.audio.R;

import android.view.Menu;
import android.view.MenuItem;
import android.widget.Button;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.Switch;
import android.media.audiofx.AcousticEchoCanceler;



/**
 * This demo will show you how to use MobileAEC class to deal with echo things
 * on android.<br>
 * </ul> <br>
 * <br>
 * <b>[NOTICE]</b>
 * <ul>
 * you should add <b>"android.permission.WRITE_EXTERNAL_STORAGE"</b> in your
 * AndroidManifest.xml file to allow this DEMO write data into your SDcard.
 * </ul>
 * <b>[TODO List]</b>
 * <ul>
 * <li>
 * (billhoo): should write all the demo processes into separate thread instead
 * of the UI thread.</li>
 * </ul>
 * 
 * @author billhoo E-mail: billhoo@126.com
 */
public class DemoActivity extends Activity implements OnClickListener {

	private RecordThread mRecordThread1;
	private PlayThread mPlayThread1;

	private RecordThread mRecordThread2;
	private PlayThread mPlayThread2;

	private Button recordButton;

	private Button record_play_Button;

	private Button playButton;

	private Switch webRTCSwitch;

	boolean startRecord =false;
	boolean startRecord_play =false;
	boolean startPlay = false;

	MobileAEC aecm;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);

		aecm= new MobileAEC(com.android.webrtc.audio.MobileAEC.SamplingFrequency.FS_8000Hz);//null);

		aecm.setAecmMode(MobileAEC.AggressiveMode.AGGRESSIVE) //MOST_AGGRESSIVE)
				.prepare();

		if (true){//AECM_DEBUG) {
		//	doAECM();

			recordButton = (Button)this.findViewById(R.id.recordbutton);
			recordButton.setOnClickListener(this);

			record_play_Button = (Button)this.findViewById(R.id.record_play_button);
			record_play_Button.setOnClickListener(this);

			playButton = (Button)this.findViewById(R.id.play_button);
			playButton.setOnClickListener(this);

			webRTCSwitch =(Switch)this.findViewById(R.id.switch1);

//        mPlay1 =(Button)this.findViewById(R.id.bt_play1);

			if (false){//AcousticEchoCanceler.isAvailable()) {
				//webRTCSwitch.setClickable(true);
				webRTCSwitch.setChecked(true);
			}
			else
			{
				webRTCSwitch.setChecked(false);
				webRTCSwitch.setClickable(false);
				webRTCSwitch.setText("No native AEC");
			}

		}
	}

	// //////////////////////////////////////////////////////////////////////////
	// ACOUSTIC ECHO CANCELLATION MOBILE EDITION

	public void doAECM() {
		try {
			MobileAEC aecm = new MobileAEC(null);
			aecm.setAecmMode(MobileAEC.AggressiveMode.MOST_AGGRESSIVE)
					.prepare();

			// get pcm raw data file in root of android sd card.
			// if you test this demo, you should put these files into your
			// android device or emulator.
			// the ideal output of pcm is almost down to zero.
			FileInputStream fin = new FileInputStream(new File(
					Environment.getExternalStorageDirectory()
							+ "/en-00-raw-pcm-16000Hz-16bit-mono.pcm"));

			FileOutputStream fout = new FileOutputStream(new File(
					Environment.getExternalStorageDirectory()
							+ "/aecm-output-pcm-16000Hz-16bit-mono.pcm"));

			final int cacheSize = 320;
			byte[] pcmInputCache = new byte[cacheSize];

			// core procession
			for (/* empty */; fin.read(pcmInputCache, 0, pcmInputCache.length) != -1; /* empty */) {
				// convert bytes[] to shorts[], and make it into little endian.
				short[] aecTmpIn = new short[cacheSize / 2];
				short[] aecTmpOut = new short[cacheSize / 2];
				ByteBuffer.wrap(pcmInputCache).order(ByteOrder.LITTLE_ENDIAN)
						.asShortBuffer().get(aecTmpIn);

				// aecm procession, for now the echo tail is hard-coded 10ms,
				// but you
				// should estimate it correctly each time you call
				// echoCancellation, otherwise aecm
				// cannot work.
				aecm.farendBuffer(aecTmpIn, cacheSize / 2);
				aecm.echoCancellation(aecTmpIn, null, aecTmpOut,
						(short) (cacheSize / 2), (short) 10);

				// output
				byte[] aecBuf = new byte[cacheSize];
				ByteBuffer.wrap(aecBuf).order(ByteOrder.LITTLE_ENDIAN)
						.asShortBuffer().put(aecTmpOut);

				fout.write(aecBuf);
			}

			fout.close();
			fin.close();
			aecm.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

//
//	@Override
//	public boolean onCreateOptionsMenu(Menu menu) {
//		// Inflate the menu; this adds items to the action bar if it is present.
//		getMenuInflater().inflate(R.menu.menu_main, menu);
//		return true;
//	}
//
//	@Override
//	public boolean onOptionsItemSelected(MenuItem item) {
//		// Handle action bar item clicks here. The action bar will
//		// automatically handle clicks on the Home/Up button, so long
//		// as you specify a parent activity in AndroidManifest.xml.
//		int id = item.getItemId();
//
//		//noinspection SimplifiableIfStatement
//		if (id == R.id.action_settings) {
//			return true;
//		}
//
//		return super.onOptionsItemSelected(item);
//	}

	@Override
	public void onClick(View v) {
		int id = v.getId();
		switch (id) {
			case R.id.recordbutton: {

				if (startRecord)
				{
					mRecordThread1.StopRecord();

					startRecord=false;
					recordButton.setText("Record");

				}
				else
				{
					startRecord=true;
					recordButton.setText("Recording");

					mRecordThread1 = new RecordThread();
					mRecordThread1.StartRecordWithoutAEC();
				}
				break;
			}
			case R.id.record_play_button: {

				if (startRecord_play)
				{
					mRecordThread2.StopRecord();

					startRecord_play=false;
					record_play_Button.setText("Record_play");

					//aecm.close();

				}
				else
				{
					mRecordThread2 = new RecordThread();
					mRecordThread2.aecm = this.aecm;
					mRecordThread2.StartRecord(webRTCSwitch.isChecked());

					mPlayThread1 = new PlayThread();
					mPlayThread1.aecm=this.aecm;
					mPlayThread1.StartPlay(mRecordThread1.getBufferList());

					startRecord_play=true;
					record_play_Button.setText("Record_playing");
				}
				break;
			}
			case R.id.play_button: {

				if (startPlay)
				{
					startPlay=false;
					playButton.setText("play");

				}
				else
				{
					mPlayThread2 = new PlayThread();
					mPlayThread2.StartPlay(mRecordThread2.getBufferList());

					startPlay=true;
					playButton.setText("playing");
				}
				break;
			}

		}
	}
}