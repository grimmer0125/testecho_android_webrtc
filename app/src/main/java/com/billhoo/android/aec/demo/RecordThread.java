package com.billhoo.android.aec.demo;

import java.util.ArrayList;

import android.media.AudioFormat;
import android.media.AudioRecord;
import android.media.MediaRecorder;

import android.media.audiofx.AcousticEchoCanceler;
import android.media.audiofx.AudioEffect.Descriptor;

import android.util.Log;

import android.media.audiofx.AudioEffect;

import com.android.webrtc.audio.MobileAEC;


public class RecordThread extends Thread{

	MobileAEC aecm;

	private static final boolean AECM_DEBUG = true;

	private AcousticEchoCanceler mAEC;

	private static final int SAMPLE_RATE = 8000;
	private ArrayList<short[]> mBufferList;
	private boolean isRecording=false;
	
	private AudioRecord mAudioRecord;

	public void StartRecordWithoutAEC()
	{
		int minBufSize = AudioRecord.getMinBufferSize(SAMPLE_RATE, AudioFormat.CHANNEL_IN_MONO, AudioFormat.ENCODING_PCM_16BIT);

		mAudioRecord = new AudioRecord(
				MediaRecorder.AudioSource.MIC,
				SAMPLE_RATE,
				AudioFormat.CHANNEL_IN_MONO,
				AudioFormat.ENCODING_PCM_16BIT,
				minBufSize);

		mBufferList  = new ArrayList<short[]>();
		isRecording=true;
		start();
	}

	public void StartRecord(boolean useAEC)
	{
		int minBufSize = AudioRecord.getMinBufferSize(SAMPLE_RATE, AudioFormat.CHANNEL_IN_MONO, AudioFormat.ENCODING_PCM_16BIT);

		if (useAEC)
		{
			mAudioRecord = new AudioRecord(
					MediaRecorder.AudioSource.VOICE_COMMUNICATION,
					SAMPLE_RATE,
					AudioFormat.CHANNEL_IN_MONO,
					AudioFormat.ENCODING_PCM_16BIT,
					minBufSize);

			initAEC(mAudioRecord.getAudioSessionId());
		}
		else
		{
			mAudioRecord = new AudioRecord(
					MediaRecorder.AudioSource.MIC,
					SAMPLE_RATE,
					AudioFormat.CHANNEL_IN_MONO,
					AudioFormat.ENCODING_PCM_16BIT,
					minBufSize);
		}

		mBufferList  = new ArrayList<short[]>();
		isRecording=true;
		start();
	}

	public boolean initAEC(int audioSession) {
		//Log.d(TAG, "initAEC audioSession:" + audioSession);
		if (mAEC != null) {
			return false;
		}

		mAEC = AcousticEchoCanceler.create(audioSession);
		int ret = mAEC.setEnabled(true);
		if (ret != AudioEffect.SUCCESS) {
			Log.i("aec","setEnabled error: " + ret);
			return false;
		}
		Descriptor descriptor = mAEC.getDescriptor();

//		Log.d(TAG, "AcousticEchoCanceler " +
//				"name: " + descriptor.name + ", " +
//				"implementor: " + descriptor.implementor + ", " +
//				"connectMode: " + descriptor.connectMode + ", " +
//				"type: " + descriptor.type + ", " +
//				"uuid: " + descriptor.uuid);

		boolean isEnable = mAEC.getEnabled();

		return  isEnable;
	}

	public boolean releaseAEC() {
		//Log.d(TAG, "releaseAEC");
		if (mAEC == null) {
			return false;
		}

		mAEC.setEnabled(false);
		mAEC.release();
		return true;
	}
	
	public void StopRecord()
	{
		isRecording=false;
	}
	
	public ArrayList<short[]> getBufferList()
	{
		return mBufferList;
	}
	
	public void run()
	{
		try {
			mAudioRecord.startRecording();
		}
		catch (Exception ex)
		{
			int k =0;
		}

		while(isRecording)
		{
			short[] buf = new short[80];
			mAudioRecord.read(buf,0, buf.length);

			if(this.aecm!=null)
			{
//				final int cacheSize = 320;
//				short[] aecTmpIn = new short[cacheSize / 2];
				short[] aecTmpOut = new short[80];

//				aecm.farendBuffer(aecTmpIn, cacheSize / 2);

				try {
					aecm.echoCancellation(buf, null, aecTmpOut,
							(short) 80, (short) 25); //25->0, x10
				}
				catch (Exception e) {

					int k = 0;
				}

				mBufferList.add(aecTmpOut);
			}
			else
			{

				mBufferList.add(buf);

			}

		}

		if (AcousticEchoCanceler.isAvailable()) {
			releaseAEC();
		}

		mAudioRecord.stop();
	}
}
