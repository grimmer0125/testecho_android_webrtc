LOCAL_PATH := $(call my-dir)
include $(CLEAR_VARS)

LOCAL_MODULE := app
LOCAL_SRC_FILES := \
	/Users/grimmer/github/aec-android2/app/src/main/jni/Android.mk \
	/Users/grimmer/github/aec-android2/app/src/main/jni/Application.mk \
	/Users/grimmer/github/aec-android2/app/src/main/jni/aecm/aecm_core.c \
	/Users/grimmer/github/aec-android2/app/src/main/jni/aecm/aecm_wrapper.c \
	/Users/grimmer/github/aec-android2/app/src/main/jni/aecm/Android.mk \
	/Users/grimmer/github/aec-android2/app/src/main/jni/aecm/complex_bit_reverse.c \
	/Users/grimmer/github/aec-android2/app/src/main/jni/aecm/complex_fft.c \
	/Users/grimmer/github/aec-android2/app/src/main/jni/aecm/cross_correlation.c \
	/Users/grimmer/github/aec-android2/app/src/main/jni/aecm/delay_estimator.c \
	/Users/grimmer/github/aec-android2/app/src/main/jni/aecm/delay_estimator_wrapper.c \
	/Users/grimmer/github/aec-android2/app/src/main/jni/aecm/division_operations.c \
	/Users/grimmer/github/aec-android2/app/src/main/jni/aecm/downsample_fast.c \
	/Users/grimmer/github/aec-android2/app/src/main/jni/aecm/echo_control_mobile.c \
	/Users/grimmer/github/aec-android2/app/src/main/jni/aecm/min_max_operations.c \
	/Users/grimmer/github/aec-android2/app/src/main/jni/aecm/randomization_functions.c \
	/Users/grimmer/github/aec-android2/app/src/main/jni/aecm/real_fft.c \
	/Users/grimmer/github/aec-android2/app/src/main/jni/aecm/ring_buffer.c \
	/Users/grimmer/github/aec-android2/app/src/main/jni/aecm/spl_init.c \
	/Users/grimmer/github/aec-android2/app/src/main/jni/aecm/spl_sqrt_floor.c \
	/Users/grimmer/github/aec-android2/app/src/main/jni/aecm/vector_scaling_operations.c \

LOCAL_C_INCLUDES += /Users/grimmer/github/aec-android2/app/src/main/jni
LOCAL_C_INCLUDES += /Users/grimmer/github/aec-android2/app/src/debug/jni

include $(BUILD_SHARED_LIBRARY)
